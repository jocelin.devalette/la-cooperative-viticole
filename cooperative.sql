CREATE TABLE `buveurs` (
        `email` varchar(50) PRIMARY KEY NOT NULL,
        `nom_buveur` varchar(50) NOT NULL,
        `prenom_buveur` varchar(50) NOT NULL,
        `mot_de_passe` int(11) NOT NULL,
        `adresse` varchar(30)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `buveurs` (`email`, `nom_buveur`, `prenom_buveur`, `mot_de_passe`, `adresse`) VALUES
        ('thibault.macquart@egd.mg', 'Thibault', 'Macquart', 12345, ''),
        ('Ostivekevin@gmail.com', 'Randrianomenjanahary', 'Ostive', 1234, 'Madagascar');

CREATE TABLE `viticulteurs` (
        `nvt` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
        `nom_viticulteur` varchar(50) NOT NULL,
        `ville_viticulteur` varchar(50)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `viticulteurs` (`nvt`, `nom_viticulteur`, `ville_viticulteur`) VALUES
        (1, 'Rothschild', 'Bordeaux'),
        (2, 'Dylan', 'Lyon');

CREATE TABLE `vins` (
        `nv` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
        `cru` varchar(50) NOT NULL,
        `annee` int(11) NOT NULL,
        `degre` float NOT NULL,
        `stock` int(11) NOT NULL,
        `nvt` int(11) NOT NULL,
        FOREIGN KEY (`nvt`) REFERENCES `viticulteurs`(`nvt`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `vins` (`nv`, `cru`, `annee`, `degre`, `stock`, `nvt`) VALUES
        (1, 'Chateau_Latour', 2001, 11.8, 63, 1),
        (2, 'Moulin_à_vent', 2010, 12.2, 48, 2);

CREATE TABLE `commande` (
        `nv` int(11) NOT NULL,
        `email` varchar(50) NOT NULL,
        `date_commande` date NOT NULL,
        `livraison_commande` tinyint(1) NOT NULL,
        `quantite` int(11) NOT NULL,
        PRIMARY KEY (`nv`,`email`),
        FOREIGN KEY (`email`) REFERENCES `buveurs`(`email`),
        FOREIGN KEY (`nv`) REFERENCES `vins`(`nv`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;